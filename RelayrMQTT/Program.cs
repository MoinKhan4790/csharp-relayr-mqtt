﻿using Newtonsoft.Json;
using relayr_csharp_sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace RelayrMQTT
{

    class StatusChecker
    {
        public  bool _conversionDummyBool;
        public  double _conversionDummyDouble;
        private int invokeCount;
        private int maxCount;

        public StatusChecker(int count)
        {
            invokeCount = 0;
            maxCount = count;
        }

        public  string createContentJson(Dictionary<string, string> content)
        {
            StringBuilder jsonString = new StringBuilder();
            bool first = true;
            jsonString.Append("{ ");
            foreach (KeyValuePair<string, string> pair in content)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    jsonString.Append(", ");
                }

                jsonString.Append("\"").Append(pair.Key).Append("\" : ");

                // ############### GIANT HACK NOT FOR CHILDREN'S EYES ##################

                if (Boolean.TryParse(pair.Value, out _conversionDummyBool) ||
                    Double.TryParse(pair.Value, out _conversionDummyDouble))
                {
                    jsonString.Append(pair.Value);
                }
                else
                {
                    jsonString.Append("\"").Append(pair.Value).Append("\"");
                }

                // #####################################################################
            }
            jsonString.Append(" }");

            return jsonString.ToString();
        }
        // This method is called by the timer delegate.
        public void CheckStatus(Object stateInfo)
        {
            AutoResetEvent autoEvent = (AutoResetEvent)stateInfo;
            Console.WriteLine("{0} Checking status {1,2}.",
                DateTime.Now.ToString("h:mm:ss.fff"),
                (++invokeCount).ToString());

            MqttChannelManager.Initialize();
            MqttChannelManager.ConnectToBroker("0ccfc8ce-0f47-4b25-b79e-bb6b3f43c22c", "3sWJT.oZDTTF");
            string message = "25";
            bool isConnected = MqttChannelManager.IsConnected;
            Tempp tp = new Tempp();
            tp.meaning = "temperature";
            Random r = new Random();
            
            tp.value = r.Next(10, 50); 
            Dictionary<string, string> str = new Dictionary<string, string>();
            str.Add("meaning", "temperature");
            str.Add("value", Convert.ToString(tp.value));
            var json = JsonConvert.SerializeObject(tp);
            StringContent contentJson = null;

            message = createContentJson(str);

            //contentJson.Headers = "Bearer fRAKWstLhiAj8Yr9JgnV7Kipotqh5xnqQ9DZWjeTGiyH2I52Gk1djvNIEDaN6qPt";
            MqttChannelManager._client.Publish("/v1/0ccfc8ce-0f47-4b25-b79e-bb6b3f43c22c/data", Encoding.UTF8.GetBytes(message), MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE, true);


            if (invokeCount == maxCount)
            {
                // Reset the counter and signal the waiting thread.
                invokeCount = 0;
                autoEvent.Set();
            }
        }
    }
    class Program
    {
        public static bool _conversionDummyBool;
        public static double _conversionDummyDouble;
        public static int previousHour = 0;
        static void Main(string[] args)
        {
            previousHour = DateTime.Now.Hour;
           
            //    mqttcall();
            // Create an AutoResetEvent to signal the timeout threshold in the
            // timer callback has been reached.
            var autoEvent = new AutoResetEvent(false);

            var statusChecker = new StatusChecker(10);

            // Create a timer that invokes CheckStatus after one second, 
            // and every 1/4 second thereafter.
            Console.WriteLine("{0:h:mm:ss.fff} Creating timer.\n",
                              DateTime.Now);
            var stateTimer = new System.Threading.Timer(statusChecker.CheckStatus,
                                       autoEvent, 1000, 10000);

            // When autoEvent signals, change the period to every half second.
            autoEvent.WaitOne();
           // stateTimer.Change(0, 500);
            Console.WriteLine("\nChanging period to .5 seconds.\n");

            // When autoEvent signals the second time, dispose of the timer.
            autoEvent.WaitOne();
            stateTimer.Dispose();
            Console.WriteLine("\nDestroying timer.");

        }

     
        public static void Callback(object source)
        {
            if (previousHour < DateTime.Now.Hour || (previousHour == 23 && DateTime.Now.Hour == 0))
            {
                previousHour = DateTime.Now.Hour;
              
            }

            //Do the stuff you want to be done every hour;
        }

        public static string createContentJson(Dictionary<string, string> content)
        {
            StringBuilder jsonString = new StringBuilder();
            bool first = true;
            jsonString.Append("{ ");
            foreach (KeyValuePair<string, string> pair in content)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    jsonString.Append(", ");
                }

                jsonString.Append("\"").Append(pair.Key).Append("\" : ");

                // ############### GIANT HACK NOT FOR CHILDREN'S EYES ##################

                if (Boolean.TryParse(pair.Value, out _conversionDummyBool) ||
                    Double.TryParse(pair.Value, out _conversionDummyDouble))
                {
                    jsonString.Append(pair.Value);
                }
                else
                {
                    jsonString.Append("\"").Append(pair.Value).Append("\"");
                }

                // #####################################################################
            }
            jsonString.Append(" }");

            return jsonString.ToString();
        }
        static  async void mqttcall()
        {
            Device result = await MqttChannelManager.SubscribeToDeviceDataAsync("0ccfc8ce-0f47-4b25-b79e-bb6b3f43c22c", QualityOfService.AtLeastOnce);

        }


    }
}
