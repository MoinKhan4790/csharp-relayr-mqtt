﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using relayr_csharp_sdk;
using Newtonsoft.Json;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClientTestApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 

    public sealed partial class MainPage : Page
    {
        public static int Value = 15;

        public MainPage()
        {
            this.InitializeComponent();

            Relayr.OauthToken = "QquIpEkAtpUcGjddFaG3OAFIjfG4YHkvYqZVqcPHBD0TsB8D8CiV0ePHky9tk3le";
            // (Un)comment the method you need to test
            //TestClient();
            //My custom functions
            GroupDevices();
            postDeviceTemp();
        }


      

        public async void GroupDevices()
        {
            //Relayr.OauthToken = "QquIpEkAtpUcGjddFaG3OAFIjfG4YHkvYqZVqcPHBD0TsB8D8CiV0ePHky9tk3le";
            //var devices = await Relayr.GetDevicesAsync(); //All Devices
            var devices2 = await Relayr.GetGroupDevicesAsync(); //Group devices
            List<RootObjectDevices> group = new List<RootObjectDevices>();
            try {
                var deveces =Convert.ToString(devices2[0]["devices"]);
                group = JsonConvert.DeserializeObject<List<RootObjectDevices>>(deveces);
            }
            catch (Exception ex)
            {
            }
        }

        public async void postDeviceTemp()
        {

            string deviceID = "43fd4c6c-8cb7-4148-b95f-96a7a2b1e841";
            Tempp tp = new Tempp();
            tp.meaning = "temperature";
            tp.value = Value;
            await Relayr.Postdevicedata(tp, deviceID); 
        }

        public class Tempppp
        {
            public string meaning { get; set; }
            public int value { get; set; }
        }
        public class RootObjectDevices
        {
            public string id { get; set; }
            public string name { get; set; }
            public string modelId { get; set; }
            public string firmwareVersion { get; set; }
            public string secret { get; set; }
            public string owner { get; set; }
            public bool @public { get; set; }
            public string integrationType { get; set; }
            public int position { get; set; }
        }
        public async void TestClient()
        {
            Relayr.OauthToken = "QquIpEkAtpUcGjddFaG3OAFIjfG4YHkvYqZVqcPHBD0TsB8D8CiV0ePHky9tk3le";
            var Transmitters = await Relayr.GetTransmittersAsync();
            
            var transmitterInfo = Transmitters[0];
            var transmitter = Relayr.ConnectToBroker(transmitterInfo, "wxQ42SS6CMh.z5sElBzwNHKKfM3noEv2");//YOUR OAUTH CLIENT ID
            var devices = await transmitter.GetDevicesAsync();

            Device d = await transmitter.SubscribeToDeviceDataAsync(devices[3]);
            d.PublishedDataReceived += d_PublishedDataReceived;

        }

           void d_PublishedDataReceived(object sender, PublishedDataReceivedEventArgs args)
        {
            // Do something with the data. The data is part of 'args'
        }
    }
}
